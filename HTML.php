<!DOCTYPE html>
<html>
<head>
<meta charset = "utf-8">
<link rel="shortcut icon" href="Logo.png" type="image/png">
<title>HTML is great!</title>
</head>
<body style = "background-color:LightGoldenrodYellow"  /> <!-- То же самое: bgcolor = "#FAFAD2" -->

<header>
<?php
include("menu2.php");
?>
</header>

<p align="center">
1. Hello World!
</p>

<p>2. Проверка работы параграфов:</p>
<p>а эта строчка будет ниже!</p>
уже эта строка полюбому будет ниже... Опа <br/> перенос

<br/>

<p> 3. Форматирование текстов:<br/>
Обычный текст :: 
<b> Жирный шрифт </b> :: 
<big>Крупный шрифт </big> :: 
<i>Курсив </i> :: 
<small>Мелкий шрифт </small> :: 
<strong>Важный (не просто жирный) шрифт </strong> :: 
1<sub>Подстрочный </sub> :: 
1<sup>Надстрочный </sup> :: 
<ins>Вставленный текст</ins> :: 
<del>Удаленный текст</del>
</p>

<p>4. Работа с заголовками:</p>
<h1>H1</h1>
<h2>H2</h2>
<h3>H3</h3>
<h4>H4</h4>
<h5>H5</h5>
<h6>H6</h6>

<p align = "center">
5. Ниже будут 2 горизонтальные линии:
<hr width = "150px" align = "right"/>
<hr width = "50%" align = "left"/>
</p>

<p>
5. Комментарии:<br/>
Вы <!-- здесь --> увидите <!-- только --> то, что я <!-- не очень --> хочу <!-- Вам --> показать.
</p>

<p>6. Изображения: </p>
Изображение сервера

<p align = "right">
<img  src = "06d3e0f9cf21e5xl.jpg" height = "100px" width = "160px" 
align = "center" border = "2px" alt = "Тестовое изображение" title = "Тестовое изображение"/>
</p>
Изображение интернета
<p align = "center">
<a href = "http://www.vk.com" target = "_blank">
<img src = "https://pp.vk.me/c630626/v630626464/2e4b6/ys1rk7d4ZWU.jpg" 
height = "25%" width = "25%" border = "1px" alt = "Изображение интернета" title = "Hello world!"/>
<a/>
</br>
<i>Это изображение я когда-то использовал как аватар</i>
</p>

<p>7. Ссылки: </p>

Пример <a href = "http://htmlbook.ru/html/a" title = "Пример" target = "_blank"><ins>ссылок</ins></a> Вы можете встретить в предыдущем пункте, 
нажав на второе изображение

<p>7. Списки и таблицы: </p>
<p>
<table border = "0" align = "center" >
 <tr>
  <td bgcolor = "orange"> &nbsp;Немерованный список&nbsp; </td>
  <td bgcolor = "orange" align = "center">Ненумерованный</td>
  <td bgcolor = "orange" align = "center">список</td>
  <td rowspan = "2" bgcolor = "yellow" align = "center"> Эта</br>ячейка</br>&nbsp;охватывает&nbsp;</br>2 строки</td>
 </tr>
 <tr>
  <td bgcolor = "blue" >
    <ol>
      <li>Раз</li>
      <li>Два</li>
      <li>Три</li>
    </ol>
  </td>
 
  <td colspan = "2" bgcolor = "blue" >
  &nbsp;Эта ячейка охватывает 2 столбца&nbsp;
    <ul>
      <li>Раз</li>
      <li>Два</li>
      <li>Три</li>
    </ul>
   </td>
 </tr>
</table>
</p>

<p>8. Элементы изменения стиля текста и блоков</p>

<p>
<!-- <div style = "Background-color:Salmon; padding:0px; "> -->
<h2><font color = "red">span</font></h2>
<h3>
Здесь среднее 
<span style = "Background-color:LightGoldenrodYellow; color:Navy; padding:2px">
слово
</span>

 должно быть выделено 
</h3>
<h2><font color = "red">font</font></h2>
<h3>
Здесь среднее 
<font color = "#000080">
слово
</font>
 должно быть выделено
</h3>
<!-- </div> -->
</p>

<p>9. Формы</p>

<form>
<table>
<tr>
<td align = "right">Логин:</td>
<td><input type = "text" name = "Логин" placeholder = "Имя пользователя" size = "15px"/></td>
</tr>
<tr>
<td align = "right">Пароль:</td>
<td><input type = "password" name = "Пароль" placeholder = "Пароль" size = "15px"/></td>
</tr>
</table>
<input type = "submit" name = "enter" value = "Войти"/>
</br>
</br>
Пол: 
</br>
<input type = "radio" name = "Пол" value = "Мужской"/>Муж</input>
<input type = "radio" name = "Пол" value = "Женский"/>Жен</input>
</br>
</br>
Чекбоксы:
</br>
<input type = "checkbox" name = "check" value = "1">Первый</input>
</br>
<input type = "checkbox" name = "check" value = "2">Второй</input>
</br>
<input type = "checkbox" name = "check" value = "3">Третий</input>
</form>

<p>10. Фреймы</p>

<!--
<iframe src = "http://test/sravnenie0.php"/>
-->














</br>
</br>
</br>
</br>
</br>
</br>
</br>
<hr align = "center" width = "75%"/>
<hr align = "center" width = "50%"/>
<hr align = "center" width = "25%"/>



















</body>
</html>